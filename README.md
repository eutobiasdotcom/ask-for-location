# Ask For Location - Cordova Plugin #

This plugins allow hybrid cordova apps to switch for location settings and ask user to turn on/off GPS

### Install the plugin ###

```
cordova plugin add https://eutobias@bitbucket.org/eutobias/ask-for-location.git
```


### Call this in your code, to switch view ###

```
AskForLocation.go();
```