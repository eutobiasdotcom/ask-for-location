import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CallbackContext;
import android.util.Log;
import android.provider.Settings;
import android.content.Intent;
import org.json.JSONArray;
import org.json.JSONException;

public class AskForLocation extends CordovaPlugin {
	public static final String TAG = "AskForLocation Plugin";
	/**
	* Constructor.
	*/
	public AskForLocation() {}
		/**
		* Sets the context of the Command. This can then be used to do things like
		* get file paths associated with the Activity.
		*
		* @param cordova The context of the main Activity.
		* @param webView The CordovaWebView Cordova is running in.
		*/
		public void initialize(CordovaInterface cordova, CordovaWebView webView) {
			super.initialize(cordova, webView);
			Log.v(TAG,"Init AskForLocation Plugin");
		}
		public boolean execute(final String action, JSONArray args, CallbackContext callbackContext) throws JSONException {

			Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
	        cordova.getActivity().startActivity(settingsIntent);
			
			return true;
		}
	}
